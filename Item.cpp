#include "Item.h"

Item::Item(std::string name, std::string num, double price)
{
	this->_name = name;
	this->_serialNumber = num;
	this->_count = 1;
	this->_unitPrice = price;
}

std::string Item::GetName()
{
	return this->_name;
}

std::string Item::GetSerialNumber()
{
	return this->_serialNumber;
}

int Item::GetCount()
{
	return this->_count;
}

double Item::GetUnitPrice()
{
	return this->_unitPrice;
}

void Item::SetName(std::string name)
{
	this->_name = name;
}

void Item::SetSerialNumber(std::string serialNumber)
{
	this->_serialNumber = serialNumber;
}

void Item::SetCount(int count)
{
	this->_count = count;
}

void Item::SetUnitPrice(int price)
{
	this->_unitPrice = price;
}

double Item::totalPrice()
{
	return this->_unitPrice * this->_count;
}

bool Item::operator==(Item & other)
{
	return this->_serialNumber == other.GetSerialNumber();
}

bool Item::operator<(Item & other)
{
	return this->_serialNumber < other.GetSerialNumber();
}

bool Item::operator>(Item & other)
{
	return this->_serialNumber > other.GetSerialNumber();
}
