#include "Customer.h"

Customer::Customer(std::string name)
{
	this->_name = name;
}

double Customer::totalSum() const
{
	int sum = 0;
	std::set<Item>::iterator it = this->_items.begin();
	// Iterate till the end of set
	while (it != this->_items.end())
	{
		Item a = (*it);
		sum += a.totalPrice();
		it++;
	}
	return sum;
}

void Customer::addItem(Item a)
{
	(this->_items).insert(a);
}

void Customer::removeItem(Item a)
{
	(this->_items).erase(a); 
}

std::string Customer::GetName()
{
	return this->_name;
}

void Customer::SetName(std::string name)
{
	this->_name = name;
}
