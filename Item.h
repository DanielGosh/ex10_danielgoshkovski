#pragma once
#include <iostream>
class Item
{
private:
	std::string _name;
	std::string _serialNumber;
	int _count; //default is 1, can never be less than 1!
	double _unitPrice; //always bigger than 0!
public:
	Item(std::string name, std::string num, double price);
	~Item();
	std::string GetName();
	std::string GetSerialNumber();
	int GetCount();
	double GetUnitPrice();
	void SetName(std::string name);
	void SetSerialNumber(std::string serialNumber);
	void SetCount(int count);
	void SetUnitPrice(int price);
	double totalPrice();
	bool operator==(Item& other);
	bool operator<(Item& other);
	bool operator>(Item& other);
};