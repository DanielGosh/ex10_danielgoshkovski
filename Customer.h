#pragma once
#include"Item.h"
#include<set>

class Customer
{
private:
	std::string _name;
	std::set<Item> _items;

public:
	Customer(std::string name);
	~Customer();
	double totalSum() const;//returns the total sum for payment   x
	void addItem(Item a);//add item to the set
	void removeItem(Item a);//remove item from the set
	std::string GetName();
	void SetName(std::string name);
};